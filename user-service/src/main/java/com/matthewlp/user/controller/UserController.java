package com.matthewlp.user.controller;

import com.matthewlp.user.VO.ResponseTemplateVO;
import com.matthewlp.user.entity.User;
import com.matthewlp.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;
    @PostMapping("/")
    public User saveUser(@RequestBody User user){

        return userService.saveUser(user);
    }
    @GetMapping("/{id}")
    public ResponseTemplateVO getUserWithDep(@PathVariable("id") Long userId){

        return userService.getUserWithDep(userId);
    }
}
