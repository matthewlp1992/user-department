package com.matthewlp.user.service;

import com.matthewlp.user.VO.Department;
import com.matthewlp.user.VO.ResponseTemplateVO;
import com.matthewlp.user.entity.User;
import com.matthewlp.user.repo.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;




import org.springframework.beans.factory.annotation.Value;

@Service
public class UserService {

    //the default value is 'department'
    @Value("${DOCKER_ENV_DEPARTMENT_CONTAINER_NAME:department}")
    private String DEPARTMENT_CONTAINER_NAME;


//    @Value("${DOCKER_ENV_VARIABLE:aDefaultValue}")
//    private String docker_env_variable;
    
    
    @Value("${DEPARTMENT_SERVICE_HOST:127.0.0.1}")
    private String DEPARTMENT_SERVICE_HOST;
    
    @Value("${department.service.port}")
    private Integer DEPARTMENT_SERVICE_PORT;

    

    @Autowired
    private UserRepo userRepo;
    @Autowired
    private RestTemplate restTemplate;

    public User saveUser(User user) {
        System.out.println("Class: UserService - method: saveUser ");
        return userRepo.save(user);
    }

    public ResponseTemplateVO getUserWithDep(Long userId) {
        System.out.println("Class: UserService - method: getUserWithDep ");
         System.out.println(" ############################## ");       
                  //DEPARTMENT_SERVICE_HOST: valore passato da container oppure impostato con valore default
         System.out.println(" DEPARTMENT_SERVICE_HOST: " + DEPARTMENT_SERVICE_HOST); 
         
         //DEPARTMENT_SERVICE_PORT: valore recupersato dal file di properties      
         System.out.println(" DEPARTMENT_SERVICE_PORT: " + DEPARTMENT_SERVICE_PORT); 
//         System.out.println(" DEPARTMENT_SERVICE_URL: " + DEPARTMENT_SERVICE_URL);     
//         System.out.println(" docker_env_variable: " + docker_env_variable);  
         System.out.println(" DEPARTMENT_CONTAINER_NAME: " + DEPARTMENT_CONTAINER_NAME);  
         System.out.println(" ############################## ");       

        
        ResponseTemplateVO vo = new ResponseTemplateVO();
        User user = userRepo.findByUserId(userId);

        //Department department = restTemplate.getForObject("http://localhost:8080/deps/" + user.getDepId(), Department.class);
        //Department department = restTemplate.getForObject("http://" + DEPARTMENT_SERVICE_HOST + ":" + DEPARTMENT_SERVICE_PORT + "/deps/" + user.getDepId(), Department.class);
        //Department department = restTemplate.getForObject(DEPARTMENT_SERVICE_URL + "/deps/" + user.getDepId(), Department.class);

        //Department department = restTemplate.getForObject("http://" + "department" + ":" + DEPARTMENT_SERVICE_PORT + "/deps/" + user.getDepId(), Department.class);
        Department department = restTemplate.getForObject("http://" + DEPARTMENT_CONTAINER_NAME + ":" + DEPARTMENT_SERVICE_PORT + "/deps/" + user.getDepId(), Department.class);



        vo.setUser(user);
        vo.setDepartment(department);

        return vo;
    }
}
