package com.matthewlp.department.repo;

import com.matthewlp.department.entity.Department;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DepRepo extends JpaRepository<Department,Long> {

    Department findByDepId(Long depId);

}
